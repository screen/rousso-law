![alt tag](images/repo-logo.jpg)

# Mark Rousso - Law Firm - Wordpress Custom Theme #

* Version: 1.0.0
* Design: [SMG](https://screenmediagroup.com/?utm_source=github_link&utm_medium=link&utm_content=rousso)
* Development: [SMG](https://screenmediagroup.com/?utm_source=github_link&utm_medium=link&utm_content=rousso)

Tema diseñado por [SMG](https://screenmediagroup.com/?utm_source=github_link&utm_medium=link&utm_content=rousso) para Mark Rousso - Law Firm.
Este tema custom fue construido en su totalidad, pasando por su etapa de Wireframing, rearmado, version anterior e implementación en hosting externo.

### Componentes Principales ###

* Twitter Bootstrap 5.0.2

### Funciones Incluídas ###

* Custom Post Type.
* Custom Taxonomies.
* Bootstrap Ready: Wordpress Menu Structure.
* Custom Metabox.

### Plugins Requeridos ###

* CMB2
* Jetpack by Wordpress

### Instrucciones de Instalación ###

1. Instalar los plugins requeridos.

2. Activar los plugins requeridos.

3. Instalar el tema via FTP o por el instalador de themes de Wordpress via zip

### Contacto ###

Soporte Oficial para este tema:

Repo Owner: [SMG](https://screenmediagroup.com/?utm_source=github_link&utm_medium=link&utm_content=rousso)

Main Developer: [SMG](https://screenmediagroup.com/?utm_source=github_link&utm_medium=link&utm_content=rousso)
