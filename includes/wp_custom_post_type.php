<?php

if (!defined('ABSPATH')) {
	die('Invalid request.');
}

/*
function rousso_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'rousso' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'rousso' ),
		'menu_name'             => __( 'Clientes', 'rousso' ),
		'name_admin_bar'        => __( 'Clientes', 'rousso' ),
		'archives'              => __( 'Archivo de Clientes', 'rousso' ),
		'attributes'            => __( 'Atributos de Cliente', 'rousso' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'rousso' ),
		'all_items'             => __( 'Todos los Clientes', 'rousso' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'rousso' ),
		'add_new'               => __( 'Agregar Nuevo', 'rousso' ),
		'new_item'              => __( 'Nuevo Cliente', 'rousso' ),
		'edit_item'             => __( 'Editar Cliente', 'rousso' ),
		'update_item'           => __( 'Actualizar Cliente', 'rousso' ),
		'view_item'             => __( 'Ver Cliente', 'rousso' ),
		'view_items'            => __( 'Ver Clientes', 'rousso' ),
		'search_items'          => __( 'Buscar Cliente', 'rousso' ),
		'not_found'             => __( 'No hay resultados', 'rousso' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'rousso' ),
		'featured_image'        => __( 'Imagen del Cliente', 'rousso' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'rousso' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'rousso' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'rousso' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'rousso' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'rousso' ),
		'items_list'            => __( 'Listado de Clientes', 'rousso' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'rousso' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'rousso' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'rousso' ),
		'description'           => __( 'Portafolio de Clientes', 'rousso' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'rousso_custom_post_type', 0 );
*/
